# SIMPLE INTERRUPT EXAMPLE#

creare un'applicazione Java composta da 3 thread.

* Un thread principale `MainThread` crea ed
avvia due altri thread `ThreadA` e `ThreadB`.
* Dopo avere atteso qualche secondo invia un `interrupt()` ad entrambi i
thread lanciati.
* Il `ThreadA` ricevere il segnale mentre è bloccato in `sleep`.
* Il `ThreadB` riceve il segnale mentre è in `running`.
* Entrambi i thread dopo avere ricevuto il segnale stampano su
**stdout** un messaggio opportuno e poi terminano l’esecuzione.