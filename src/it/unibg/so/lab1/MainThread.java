package it.unibg.so.lab1;

public class MainThread {

	public static void main(String[] args) {
		
		System.out.println("[thread Main]: sono in running.");
				
		Thread a = new ThreadA("A");
		Thread b = new ThreadB("B");
		
		a.start();
		b.start();
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		a.interrupt();
		b.interrupt();
		
		System.out.println("[thread Main]: ho inviato gli interrupt.");
	}

}
