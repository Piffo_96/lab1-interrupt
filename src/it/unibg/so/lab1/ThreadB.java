package it.unibg.so.lab1;

public class ThreadB extends Thread {

	public ThreadB(String n) {
		setName(n);
	}

	@Override
	public void run() {
		System.out.println("[thread " + getName() + "]: sono in running.");
		
		while (true) {
			compute();
			if(Thread.interrupted()){
				System.out.println("[thread " + getName() + "]: eccezione ricevuta mentre ero in running.");
				break;
			}
		}
	}
	
	private void compute() {
		for(int i=2; i<1000; i++){
			boolean prime = true;
			int j=2;
			while(j<i){
				if(i%j == 0){
					prime = false;
					break;
				}
				j++;
			}
			if(prime);
		}
				
	}
	
}
